<?
$h1         = 'Bateria Cia';
$title      = 'Bateria Cia';
$desc       = 'Faça cotações para diversos serviços de baterias, com várias empresas, em um só lugar!';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>BATERIAS</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>AUTOMOTIVAS</h2>
        <p>Corrosão é um dos fatores decisivos para o usuário adquirir uma nova bateria. Em inúmeros casos, comprar uma bateria automotiva é a solução ideal para o usuário até porque uma corrosão nos terminais pode ser fatal, impedindo a partida de um carro devido à resistência elétrica do mesmo. O custo do produto vai depender da corrente elétrica que ela é capaz de proporcionar.</p>
        <a href="<?=$url?>bateria-automotiva" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>TRACIONÁRIA</h2>
        <p>Quando se trata de uma bateria, é vital que seja realizado um teste por um mecânico trimestralmente a partir da data de compra. Quando for necessária a troca, é importante contar com a ajuda de um profissional caso não se tenha ideia de qual comprar. Mas é imprescindível se certificar sobre o tamanho certo e os locais dos terminais para o carro.</p>
        <a href="<?=$url?>bateria-tracionaria" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>CARREGADOR</h2>
        <p>Além da bateria, o cliente também pode encontrar no mercado o carregador bateria que tem como principal finalidade recarregar a bateria para que a mesma tenha um desempenho similar ao anterior. Este aparelho tem grande uso pelos mais diversos tipos de segmentos, com grande destaque para as empresas do ramo automobilístico e mecânico.</p>
        <a href="<?=$url?>carregador-de-bateria-estacionaria" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>

<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>BATERIAS</h2>
        <div class="div-img">
          <p data-anime="left-0">Antes do cliente adquirir uma bateria estacionária nova, é importante que ele entenda o funcionamento do produto. Essa bateria é muito conhecida no mercado deste segmento, que nada mais é que um tipo de bateria construída de forma semelhante a bateria automotiva comum, mas com uma maior capacidade de descarga. Essa é uma das inúmeras baterias existentes no mercado!</p>
        </div>
        <div class="gerador-svg" data-anime="in">
          <img src="imagens/img-home/baterias.jpg" alt="Baterias" title="Baterias">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>No mercado existem inúmeras opções de baterias, diferenciando por modelos, amperes, tamanhos, aplicações, justamente pelo leque de opções que existem neste segmento, como automóveis de lazer, caminhões, entre outros. Tudo vai depender das necessidades dos compradores, sempre visando atender suas preferências e expectativas. Seguem algumas opções:</p>
            
            <li><i class="fas fa-angle-right"></i>Bateria tracionaria</li>
            <li><i class="fas fa-angle-right"></i>Bateria de caminhão</li>
            <li><i class="fas fa-angle-right"></i>Bateria de trator</li>
            <li><i class="fas fa-angle-right"></i>Entre outras.</li>
          </ul>
          <a href="<?=$url?>baterias-industriais" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-dollar-sign fa-7x"></i>
            <div>
              <p>Compare preços com diversos fornecedores</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="far fa-handshake fa-7x"></i>
            <div>
              <p>Faça o melhor negócio para sua empresa</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="far fa-thumbs-up fa-7x"></i>
            <div>
              <p>Fabricantes de confiança</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-bolt fa-7x"></i>
            <div>
              <p>Energia armazenada</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-calendar fa-7x"></i>
            <div>
              <p>Encontre as melhores baterias do mercado</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-chart-line fa-7x"></i>
            <div>
              <p>Aumente sua eficiência</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>bateria-automotiva">
              <div class="fig-img">
                <h2>BATERIA AUTOMOTIVA</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>bateria-estacionaria">
              <div class="fig-img2">
                <h2>BATERIA ESTACIONÁRIA</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>carregador-de-bateria-preco">
              <div class="fig-img">
                <h2>CARREGADOR DE BATERIA</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
        <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria-baterias.jpg" class="lightbox" title="Baterias" >
            <img src="<?=$url?>imagens/img-home/thumbs/galeria-baterias.jpg" title="Baterias" alt="Baterias">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria-calibracoes-de-baterias.jpg" class="lightbox"  title="Calibrações de baterias">
            <img src="<?=$url?>imagens/img-home/thumbs/galeria-calibracoes-de-baterias.jpg" alt="Calibrações de baterias" title="Calibrações de baterias">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria-carregador-de-bateria.jpg" class="lightbox" title="Carregador de bateria" >
            <img src="<?=$url?>imagens/img-home/thumbs/galeria-carregador-de-bateria.jpg" alt="Carregador de bateria" title="Carregador de bateria">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/galeria-baterias-industriais.jpg" class="lightbox" title="Baterias industriais">
            <img src="<?=$url?>imagens/img-home/thumbs/galeria-baterias-industriais.jpg" alt="Baterias industriais" title="Baterias industriais">
            </a>
         	</li>
         	<li><a href="<?=$url?>imagens/img-home/galeria-baterias-automotivas.jpg" class="lightbox" title="Cotar Baterias automotivas" >
            <img src="<?=$url?>imagens/img-home/thumbs/galeria-baterias-automotivas.jpg" alt="Baterias automotivas"  title="Baterias automotivas">
         	</a>
      		</li>
     	 	<li><a href="<?=$url?>imagens/img-home/galeria-dessulfatador-de-bateria.jpg" class="lightbox" title="Dessulfatador de bateria" >
         	<img src="<?=$url?>imagens/img-home/thumbs/galeria-dessulfatador-de-bateria.jpg" alt="Dessulfatador de bateria" title="Dessulfatador de bateria">
      		</a>
   			</li>
   			<li><a href="<?=$url?>imagens/img-home/galeria-bateria-tracionaria.jpg" class="lightbox" title="Bateria tracionária" >
      		<img src="<?=$url?>imagens/img-home/thumbs/galeria-bateria-tracionaria.jpg" alt="Bateria tracionária" title="Bateria tracionária">
   			</a>
			</li>
			<li><a href="<?=$url?>imagens/img-home/galeria-descarregador-de-baterias.jpg" class="lightbox" title="Descarregador de baterias"  >
   			<img src="<?=$url?>imagens/img-home/thumbs/galeria-descarregador-de-baterias.jpg" alt="Descarregador de baterias" title="Descarregador de baterias">
			</a>
			</li>
			<li><a href="<?=$url?>imagens/img-home/galeria-carregador-de-bateria-de-carro.jpg" class="lightbox" title="Carregador de bateria de carro" >
			<img src="<?=$url?>imagens/img-home/thumbs/galeria-carregador-de-bateria-de-carro.jpg" alt="Carregador de bateria de carro" title="Carregador de bateria de carro">
			</a>
			</li>
			<li><a href="<?=$url?>imagens/img-home/galeria-bateria-para-empilhadeira.jpg" class="lightbox" title="Bateria para empilhadeira" >
			<img src="<?=$url?>imagens/img-home/thumbs/galeria-bateria-para-empilhadeira.jpg" alt="Bateria para empilhadeira" title="Bateria para empilhadeira">
			</a>
			</li>
		</ul>
		</div>
	</div>
</section>
</main>
<? include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>