<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form id="demo-form2" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
        <div class="page-title">
            <div class="title_left">
                <h3>Edição de colaboradores</h3> 
            </div>
            <div class="clearfix"></div>                   
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">         
                    <div class="pull-right">
                        <button type="submit" name="UpdateUser" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div> 
                    <br/>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    
                    if (isset($post) && isset($post['UpdateUser'])):
                        $post['user_cover'] = ( $_FILES['user_cover']['tmp_name'] ? $_FILES['user_cover'] : null );
                        $post['empresa_id'] = $empresa_id;
                        unset($post['UpdateUser']);

                        $update = new AdminProfile;
                        $update->ExeUpdate($_SESSION['userlogin']['user_id'], $post);

                        if (!$update->getResult()):
                            $erro = $update->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            header('Location: painel.php?exe=profile/update&get=true');
                        endif;
                    endif;

                    if (isset($get) && $get == true && !isset($post)):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro('Dados do usuário atualizado com sucesso.', WS_ACCEPT, null, 'MPI Technology');
                    endif;
                    ?>                        
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">                            
                                    <h2>Dados pessoais</h2>    
                                    <div class="clearfix"></div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_cover">Foto do perfil<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="file" id="app_cover" name="user_cover">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtFirstName">Nome <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtFirstName" type="text" name="user_name" value="<?php
                                        if (isset($post['user_name'])): echo $post['user_name'];
                                        else: echo $user_name;
                                        endif;
                                        ?>" placeholder="Digite o nome" required="required" data-rule-rangelength="[1,30]" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtLastName">Sobrenome <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtLastName" type="text" name="user_lastname" value="<?php
                                        if (isset($post['user_lastname'])): echo $post['user_lastname'];
                                        else: echo $user_lastname;
                                        endif;
                                        ?>" placeholder="Digite o sobrenome" required="required" data-rule-rangelength="[1,50]" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtCustomerName">Cargo <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtCustomerName" type="text" name="user_cargo" value="<?php
                                        if (isset($post['user_cargo'])): echo $post['user_cargo'];
                                        else: echo $user_cargo;
                                        endif;
                                        ?>" placeholder="Digite o cargo" required="required" data-rule-rangelength="[1,50]" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtDatePicker">Data de nascimento <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div data-format="DD/MM/YYYY" class="input-group">
                                            <input id="j_nasc" type="text" data-mask="99/99/9999" required="required" name="user_nasc" value='<?php
                                            if (isset($post['user_nasc'])): echo $post['user_nasc'];
                                            else: echo date("d/m/Y", strtotime($user_nasc));
                                            endif;
                                            ?>' placeholder="00/00/0000" required="required" data-mask="99/99/9999" data-rule-minlength="8" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtPhoneNumber">Telefone <span class="text-info">(opcional)</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtPhoneNumber" data-mask="(99) 9 99999999" type="text" name="user_fone" value="<?php
                                        if (isset($post['user_fone'])): echo $post['user_fone'];
                                        else: echo $user_fone;
                                        endif;
                                        ?>" placeholder="(99) 9 9999-9999" data-rule-phonenumber="true" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtDigits">Ramal <span class="text-info">(opcional)</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtDigits" type="text" name="user_ramal" value="<?php
                                        if (isset($post['user_ramal'])): echo $post['user_ramal'];
                                        else: echo $user_ramal;
                                        endif;
                                        ?>" placeholder="Digite o ramal"  data-rule-digits="true" data-mask="99999" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_about">Sobre o usuário</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <textarea name="user_about" id="user_about" placeholder="Conte um pouco sobre a sua história" rows="3" class="form-control"><?php
                                            if (isset($post['user_about'])): echo $post['user_about'];
                                            else: echo $user_about;
                                            endif;
                                            ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_theme">Tema <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select name="user_theme" class="form-control col-md-7 col-xs-12" required="required">
                                            <option value="Grid" <?php
                                            if ($user_theme == "Grid"): echo 'selected'; endif;
                                            ?>>1 - Grid</option>  
                                            <option value="List" <?php
                                            if ($user_theme == "List"): echo 'selected'; endif;
                                            ?>>2 - List</option>  
                                            <option value="Full" <?php
                                            if ($user_theme == "Full"): echo 'selected'; endif;
                                            ?>>3 - Full</option>  
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_genero">Gênero <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div class="radio">
                                            <label>
                                                <input name="user_genero" id="optionsRadios1" required="required" type="radio" value="Masculino" <?php
                                                if ($user_genero == 'Masculino'): echo 'checked="true"';

                                                endif;
                                                ?>> Masculino
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="user_genero" id="optionsRadios2" type="radio" value="Feminino" <?php
                                                if ($user_genero == 'Feminino'): echo 'checked="true"';
                                                endif;
                                                ?>>Feminino
                                            </label>
                                        </div>                 
                                    </div>                                 
                                </div> 

                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">                            
                                    <h2>Dados de acesso</h2>    
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtEmail">E-mail <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtEmail" type="text" name="user_email" value="<?php
                                        if (isset($post['user_email'])): echo $post['user_email'];
                                        else: echo $user_email;
                                        endif;
                                        ?>" placeholder="Digite o e-mail" required="required" data-rule-rangelength="[10,70]" data-rule-email="true" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtPassword">Senha</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtPassword" type="password" name="user_password" placeholder="Senha"  data-rule-rangelength="[6,30]" class="form-control" autocomplete="false">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtConfirmPassword">Confirme a senha</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input id="txtConfirmPassword" type="password" name="txtConfirmPassword" placeholder="Confirme a senha" data-rule-rangelength="[6,30]" class="form-control" autocomplete="false">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>              
                <!-- //FIM DO UPDATE PERFIL -->          
            </div>
        </div>
    </form>
</div>
