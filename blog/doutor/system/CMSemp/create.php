<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 5;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form id="demo-form2" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
        <div class="page-title">
            <div class="title col-md-12 col-sm-6 col-xs-12">
                <h3><i class="fa fa-briefcase"></i> Cadastro de empresa</h3>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pull-right">
                        <button type="submit" name="CadastraEmp" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=CMSemp/index'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div> 
                    <br/>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['CadastraEmp'])):

                        $post['user_level'] = 3;
                        $post['empresa_capa'] = ( isset($_FILES['empresa_capa']['tmp_name']) ? $_FILES['empresa_capa'] : null );
                        unset($post['CadastraEmp']);

                        $cadastra = new AdminCMS;
                        $cadastra->ExeCreate($post);

                        if (!$cadastra->getResult()):
                            $erro = $cadastra->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            $_SESSION['Error'] = $cadastra->getError();
                            header('Location: painel.php?exe=CMSemp/create&get=true');
                        endif;
                    endif;

                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;
                    ?>                   
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">                                                                
                                    <h2>Dados da empresa</h2>    
                                    <div class="clearfix"></div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_capa">Logo da empresa <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_capa" id="app_cover" required="required" type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_name">Nome fantasia <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_name" id="empresa_name" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_name'])): echo $post['empresa_name'];
                                        endif;
                                        ?>" placeholder="Digite o nome fantasia da empresa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_razaosocial">Razão social <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_razaosocial" id="empresa_razaosocial" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_razaosocial'])): echo $post['empresa_razaosocial'];
                                        endif;
                                        ?>" placeholder="Digite a razão social da empresa">
                                    </div>                            
                                </div>                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_cnpj">CNPJ <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_cnpj" id="empresa_cnpj" data-mask="99.999.999/9999-99" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_cnpj'])): echo $post['empresa_cnpj'];
                                        endif;
                                        ?>" placeholder="Digite o CNPJ da empresa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_ie">Inscrição estadual <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_ie" id="empresa_ie" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_ie'])): echo $post['empresa_ie'];
                                        endif;
                                        ?>" placeholder="Digite a inscrição estadual da empresa">
                                    </div>                            
                                </div>                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_ramo">Segmento <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_ramo" id="empresa_ramo" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_ramo'])): echo $post['empresa_ramo'];
                                        endif;
                                        ?>" placeholder="Digite o ramo em que a empresa atua">
                                    </div>  
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_sobre">Sobre a empresa</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <textarea name="empresa_sobre" id="empresa_sobre" placeholder="Conte um pouco sobre a história da empresa" rows="3" class="form-control"><?php
                                            if (isset($post['empresa_sobre'])): echo $post['empresa_sobre'];
                                            endif;
                                            ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="uf">Estado <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select id="uf" name="empresa_uf" class="form-control j_loadstate" required="required">
                                            <option value="" selected="true">--Selecione o estado--</option>                                    
                                            <?php
                                            $readState = new Read;
                                            $readState->ExeRead(TB_UF, "ORDER BY estado_nome ASC");
                                            foreach ($readState->getResult() as $estado):
                                                extract($estado);
                                                echo "<option value=\"{$estado_id}\" ";
                                                if (isset($post['empresa_uf']) && $post['empresa_uf'] == $estado_id): echo 'selected';
                                                endif;
                                                echo "> {$estado_uf} / {$estado_nome} </option>";
                                            endforeach;
                                            ?>  
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cidade">Cidade <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select id="cidade" name="empresa_cidade" class="form-control j_loadcity" required="required">
                                            <?php if (!isset($post['empresa_cidade'])): ?>
                                                <option value="" selected disabled> Selecione antes um estado </option>
                                                <?php
                                            else:
                                                $City = new Read;
                                                $City->ExeRead(TB_CID, "WHERE estado_id = :uf ORDER BY cidade_nome ASC", "uf={$post['empresa_uf']}");
                                                if ($City->getRowCount()):
                                                    foreach ($City->getResult() as $cidade):
                                                        extract($cidade);
                                                        echo "<option value=\"{$cidade_id}\" ";
                                                        if (isset($post['empresa_cidade']) && $post['empresa_cidade'] == $cidade_id):
                                                            echo "selected";
                                                        endif;
                                                        echo "> {$cidade_nome} </option>";
                                                    endforeach;
                                                endif;
                                            endif;
                                            ?>
                                        </select>
                                    </div>                            
                                </div>                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_endereco">Endereço <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_endereco" id="empresa_endereco" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_endereco'])): echo $post['empresa_endereco'];
                                        endif;
                                        ?>" placeholder="Digite o endereço da empresa">
                                    </div>  
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_cep">CEP <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_cep" id="empresa_cep" data-mask="99999-999" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_cep'])): echo $post['empresa_cep'];
                                        endif;
                                        ?>" placeholder="Digite o CEP da empresa">
                                    </div>  
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_fone">Telefone principal <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_fone" id="empresa_fone" data-mask="(99) 9999-9999" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_fone'])): echo $post['empresa_fone'];
                                        endif;
                                        ?>" placeholder="Digite o telefone principal da empresa">
                                    </div>                            
                                </div>                            
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_site">Site <span class="text-info">(opcional)</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_site" id="empresa_site" data-rule-url="false" data-rule-required="false" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_site'])): echo $post['empresa_site'];
                                        endif;
                                        ?>" placeholder="Digite o endereço do site da empresa http://www.site.com.br">
                                    </div>                                                                           
                                </div>                                                                           

                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">  
                                    <h2>Dados do plano</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <br />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_disco">Espaço em disco <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_disco" id="empresa_disco" data-mask="999999" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_disco'])): echo $post['empresa_disco'];
                                        endif;
                                        ?>" placeholder="Digite o espaço em disco do plano (10000 para 10MB)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_users">Número de usuários <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="empresa_users" id="empresa_users" data-mask="999999" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['empresa_users'])): echo $post['empresa_users'];
                                        endif;
                                        ?>" placeholder="Digite a quantidade de usuários do plano">
                                    </div>                                                   
                                </div>
                                <br/>
                                <div class="x_title"> 
                                    <h2>Usuário primário</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_email">E-mail <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_email" id="user_email" required="required" type="text" placeholder="Insira o e-mail do usuário" required="required" data-rule-rangelength="[10,70]" data-rule-email="true" class="form-control" value="<?php
                                        if (isset($post['user_email'])): echo $post['user_email'];
                                        endif;
                                        ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_password">Senha <span class="text-info">(gerada automaticamente)</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_password" id="user_password" required="required" type="password" readonly="true" class="form-control" value="<?= substr(md5(time() + date('d-m-Y H:i:s')), 0, 10); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_name">Nome <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_name" id="user_name" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['user_name'])): echo $post['user_name'];
                                        endif;
                                        ?>" placeholder="Digite o nome do usuário">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_lastname">Sobrenome <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_lastname" id="user_lastname" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['user_lastname'])): echo $post['user_lastname'];
                                        endif;
                                        ?>" placeholder="Digite o sobrenome do usuário">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_genero">Gênero <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div class="radio">
                                            <label>
                                                <input name="user_genero" id="optionsRadios1" required="required" type="radio" value="Masculino" <?php
                                                if ($post['user_genero'] == 'Masculino'): echo 'checked="true"';
                                                endif;
                                                ?>> Masculino
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="user_genero" id="optionsRadios2" type="radio" value="Feminino" <?php
                                                if ($post['user_genero'] == 'Feminino'): echo 'checked="true"';
                                                endif;
                                                ?>>Feminino
                                            </label>
                                        </div>                 
                                    </div>                                 
                                </div>                                 
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_cargo">Cargo <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_cargo" id="user_cargo" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['user_cargo'])): echo $post['user_cargo'];
                                        endif;
                                        ?>" placeholder="Digite o cargo do usuário">
                                    </div>  
                                </div>  
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="j_nasc">Data de nascimento <span class="required">*</span></label>                             
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div data-format="DD/MM/YYYY" class="input-group datepicker">                                    
                                            <input name="user_nasc" id="j_nasc" data-mask="99/99/9999" type="text" required="required" data-rule-minlength="8" class="form-control" value="<?php
                                            if (isset($post['user_nasc'])): echo $post['user_nasc'];
                                            endif;
                                            ?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>                                
                                    </div>                                
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_fone">Telefone</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_fone" id="user_fone" data-mask="(99) 9 99999999" required="required" type="text" class="form-control" value="<?php
                                        if (isset($post['user_fone'])): echo $post['user_fone'];
                                        endif;
                                        ?>" placeholder="Digite o telefone do usuário">
                                    </div> 
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_ramal">Ramal <span class="text-info">(opcional)</span></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input name="user_ramal" id="user_ramal" data-mask="999999" data-rule-required="false" type="text" class="form-control" value="<?php
                                        if (isset($post['user_ramal'])): echo $post['user_ramal'];
                                        endif;
                                        ?>" placeholder="Digite o ramal do usuário">
                                    </div>                              
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div> 
        <div class="pull-right">
            <button type="submit" name="CadastraEmp" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=CMSemp/index'"><i class="fa fa-home"></i></button>
        </div> 
        <div class="clearfix"></div> 
        <br/>
    </form>
</div>

