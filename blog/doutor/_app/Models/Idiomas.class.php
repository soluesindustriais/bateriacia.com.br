<?php

/**
 * Idiomas.class.php [MODEL]
 * Reponsável por cadastrar as empresas como parents em outro idioma
 * @copyright (c) 2017, Rafael da Silva Lima Web Stylus
 */
class Idiomas {

  Private $Id;
  Private $Data;
  Private $Empresa;
  Private $Error;
  Private $Result;

  public function ExeCreate($Data) {
    $this->Data = array_map('trim', $Data);
    $this->CheckUnset();
    if ($this->CheckData()):
      $this->SetEmpresa();
    endif;
  }

  public function ExeUpdate($Id, $Data) {
    $this->Id = (int) $Id;
    $this->Data = array_map('trim', $Data);
    $this->CheckUnset();
    if ($this->CheckData()):
      $this->SetNull();
      $this->Update();
    endif;
  }

  private function CheckUnset() {
    if (empty($this->Data['empresa_idioma_style'])):
      $this->Data['empresa_idioma_style'] = ' ';
    endif;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Result = false;
      $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
    elseif ($_SESSION[ 'userlogin']['user_level'] < 3):
      $this->Result = false;
      $this->Error = array("Você não está autorizado a executar esta operação, fale com o gestor do sistema.", WS_ERROR, "Alerta!");
    else:
      return true;
    endif;
  }

  private function SetEmpresa() {
    $Read = new Read;
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :emp AND empresa_parent IS NULL", "emp={$_SESSION['userlogin']['user_empresa']}");
    if (!$Read->getResult()):
      $this->Result = false;
      $this->Error = array("Não foi possível encontrar os dados da empresa principal.", WS_ERROR, "Alerta!");
    else:
      $this->Empresa = $Read->getResult()[0];
      $this->SetNull();
      $this->SetDados();
    endif;
  }

  private function SetDados() {
    $this->Empresa['empresa_parent'] = $this->Empresa['empresa_id'];
    unset($this->Empresa['empresa_id'], $this->Empresa['empresa_date'], $this->Empresa['empresa_lastupdate']);
    $this->Empresa['empresa_idioma'] = $this->Data['empresa_idioma'];
    $this->Empresa['empresa_idioma_name'] = $this->Data['empresa_idioma_name'];
    $this->Create();
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_EMP, $this->Empresa);
    if (!$Create->getResult()):
      $this->Result = false;
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
    else:
      $this->Result = true;
      $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Idiomas", "Cadastrou o idioma {$this->Data['empresa_idioma_name']}", date("Y-m-d H:i:s"));
      $this->Data = null;
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_EMP, $this->Data, "WHERE empresa_id = :id AND (empresa_id = :sess OR empresa_parent = :sess)", "id={$this->Id}&sess={$_SESSION['userlogin']['user_empresa']}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro atualizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Idiomas", "Atualizou o idioma {$this->Data['empresa_idioma_name']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      $this->Id = null;
    endif;
  }

  //Verifica os campos que estão vazios e seta eles como null para gravar no banco
  private function SetNull() {
    $this->Data = array_map('trim', $this->Data);
    foreach ($this->Data as $key => $value):
      if (empty($this->Data[$key])):
        $this->Data[$key] = null;
      endif;
    endforeach;
  }

}
