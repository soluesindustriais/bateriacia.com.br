<?php

/**
 * HomePage.class.php [MODEL]
 * Classe responsável por criar a home page do site
 * @copyright (c) 2017, Rafael da Silva Lima & Doutores da Web
 */
class HomePage {

  //Tratamento de resultados e mensagens
  private $Result;
  private $Error;
  //Entrada de dados
  private $Data;
  private $Id = null;

  /**
   * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
   * @param array $Data
   */
  public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();
    $this->CheckData();

    if ($this->Result):
      $this->Create();
    endif;
  }

  public function ExeUpdate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();

    $this->Id = $this->Data['home_id'];
    unset($this->Data['home_id']);

    $this->CheckData();

    if ($this->Result):
      $this->Update();
    endif;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################
  //Verifica se algum campo que não é obrigatorio está vazio e remove do array

  private function CheckUnset() {
    if (empty($this->Data['home_content'])):
      $this->Data['home_content'] = " ";
    endif;
    if (empty($this->Data['home_keywords'])):
      $this->Data['home_keywords'] = " ";
    endif;
    if (empty($this->Data['attr_id'])):
      $this->Data['attr_id'] = " ";
    endif;
  }

  //Verifica a integridade dos dados e direciona as operações
  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif (Check::CategoryRepeat($this->Data['home_title'], $_SESSION['userlogin']['user_empresa'], $this->Id)):
      $this->Error = array("Já existe uma página, sessão ou categoria com este mesmo titulo: <b>{$this->Data['home_title']}</b>, tente utilizar outro titulo.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->SetNull();
      $this->Result = true;
    endif;
  }

  //Verifica os campos que estão vazios e seta eles como null para gravar no banco
  private function SetNull() {
    $this->Data = array_map('trim', $this->Data);
    foreach ($this->Data as $key => $value):
      if (empty($this->Data[$key])):
        $this->Data[$key] = null;
      endif;
    endforeach;
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_HOME, $this->Data);
    if (!$Create->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Página inicial", "Cadastrou a Página inicial {$this->Data['home_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_HOME, $this->Data, "WHERE user_empresa = :emp AND home_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$this->Id}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro atualizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Página inicial", "Atualizou a página inicial {$this->Data['home_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      $this->Id = null;
    endif;
  }

}
