<?php
// DEFINIÇÕES DE MENSAGENS QUE SERÃO ENVIADAS POR E-MAIL NO SISTEMA
define("BOAS_VINDAS", "Seja bem vindo(a) ao " . SITEDESC . ".
	Esperamos oferecer-lhe o melhor de nossos serviços, aprimorando nosso atendimento a cada dia,
	e aproveitamos para informar que estamos em alerta permanente quanto ao controle de qualidade de nossos serviços,
	para que sua satisfação seja plena e nossos laços de parceria comercial sejam cada vez mais fortes.
	");
define("BOAS_VINDAS_USER", "Seja bem vindo(a) ao " . SITEDESC);
define("UPDATE_EMPRESA", "Os dados da empresa foram atualizados no " . SITEDESC);
define("UPDATE_USER", "Os dados do usuário foram atualizados no " . SITEDESC);
define("REC_SENHA", "Para recuperar sua senha de acesso ao " . SITEDESC);
define('CADASTRO_NEWSLETTER', 'Obrigado por se cadastrar em nossa lista de e-mails, sempre que possível enviaremos para você boletins com nossas últimas novidades em produtos e serviços.');