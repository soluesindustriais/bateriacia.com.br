<!-- Main Sidebar start-->
<aside>
  <div class="col-md-3 left_col">
    <div class="left_col scroll-view">
      <!-- menu profile quick info -->
      <div class="profile">
        <div class="navbar nav_title" style="border: 0;">
          <a href="<?= RAIZ; ?>" target="_blank" class="site_title" title="<?= SITENAME; ?>"><img src="<?= BASE . '/' . Check::GetEmpresaCapa($_SESSION['userlogin']['user_empresa']); ?>" title="<?= SITE_ADDR_NAME ?>" alt="<?= SITE_ADDR_NAME ?>" class="img-responsive"/></a>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="text-center">
          <a href="javascript:void(0)" style="display: inline-block; border-radius: 50%; width: 100px; height: 100px;"><?= Check::Image('../doutor/uploads/' . $user_cover, $user_name, 'img-responsive img-circle', 100, 100); ?></a>
        </div>
        <div class="clearfix"></div>
        <div class="text-center">
          <span>Bem vindo à <?= Check::GetEmpresaName($_SESSION['userlogin']['user_empresa']); ?></span>
          <h2><?= $user_name; ?></h2>
        </div>
        <div class="clearfix"></div>
        <div class="text-center">
          <h2>Último acesso em: </h2>
          <span class="text-warning"><?= Check::GetLastAccess(); ?></span>
        </div>
        <div class="clearfix"></div>
        <div class="text-center">
          <?php
          $lv = 4;
          if (!APP_USERS || empty($userlogin) || $user_level >= $lv):
            ?>
            <h2 class="text-center">Logado na empresa:</h2>
            <p class="text-center text-danger j_getEmp"><?= $empresa_razaosocial; ?></p>
            <?php if (EMPRESA_MASTER != $_SESSION['userlogin']['user_empresa']): ?>
              <button type="button" class="btn btn-primary j_view" id="<?= EMPRESA_MASTER; ?>" alt="<?= SITENAME; ?>"><i class="fa fa-undo"></i> Voltar para Doutores</button>
              <?php
            endif;
          endif;
          ?>
        </div>
        <!-- /menu profile quick info -->
        <?php if (EMPRESA_MASTER != $_SESSION['userlogin']['user_empresa']): ?>
          <div class="col-md-12 text-center">
            <h2>Uso do plano</h2>
            <?php
            $dirpercent = ( (Check::GetDirectorySize($_SESSION['userlogin']['user_empresa']) / 1024) * 100 ) / $empresa_disco;
            $dirpercent = number_format($dirpercent, 2);
            $dirpercent = floor($dirpercent);
            ?>
            <div class="text-left">Espaço em disco <span class="label label-info pull-right"><?= $dirpercent; ?>%</span></div>
            <div class="progress">
              <div class="progress-bar progress-bar-info" style="text-shadow: #000 1px 1px 1px;" data-transitiongoal="<?= $dirpercent; ?>"></div>
            </div>
            <?php
            $Readuseremp = new Read;
            $Readuseremp->ExeRead(TB_USERS, 'WHERE user_empresa = :empid', "empid={$empresa_id}");
            if ($Readuseremp->getResult()):
              $userpercent = ( $Readuseremp->getRowCount() / $empresa_users ) * 100;
              $userpercent = number_format($userpercent, 2);
              $userpercent = floor($userpercent);
              ?>
              <div class="text-left">Usuários <span class="label label-success pull-right"><?= $userpercent; ?>%</span></div>
              <div class="progress">
                <div class="progress-bar progress-bar-success" style="text-shadow: #000 1px 1px 1px;" data-transitiongoal="<?= $userpercent; ?>"></div>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>
      <br/>
      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
          <h3>Menu</h3>
          <ul class="nav side-menu">
            <li><a href="painel.php"><i class="fa fa-dashboard"></i> Início</a></li>
            <?php if (EMPRESA_MASTER == $_SESSION['userlogin']['user_empresa']): ?>
              <li><br><h3>Administrador</h3><br>
                <?php
                $lv = 4;
                if (!APP_USERS || empty($userlogin) || $user_level >= $lv):
                  ?>
                  <li class="<?php if (in_array('CMSemp', $linkto)) echo 'active'; ?>"><a><i class="fa fa-briefcase"></i> Empresa <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php if (in_array('CMSemp', $linkto)) echo 'display: block'; ?>">
                      <li class="<?php if (in_array('create', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=CMSemp/create">Cadastrar</a></li>
                      <li class="<?php if (in_array('index', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=CMSemp/index">Listar</a></li>
                    </ul>
                  </li>
                  <?php
                endif;
                $lv = 5;
                if (!APP_USERS || empty($userlogin) || $user_level >= $lv):
                  ?>
                  <li class="<?php if (in_array('CMSfunc', $linkto)) echo 'active'; ?>"><a><i class="fa fa-user"></i> Colaboradores <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php if (in_array('CMSfunc', $linkto)) echo 'display: block'; ?>">
                      <li class="<?php if (in_array('create', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=CMSfunc/create">Cadastrar</a></li>
                      <li class="<?php if (in_array('index', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=CMSfunc/index">Listar</a></li>
                    </ul>
                  </li>
                <?php endif; ?>
                <?php
                $lv = 5;
                if (!APP_USERS || empty($userlogin) || $user_level >= $lv):
                  ?>
                  <li class="<?php if (in_array('CMShist', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=CMShist/index"><i class="fa fa-th-list"></i> Histórico</a></li>
                <?php endif; ?>
              </ul>
            </div>
            <?php
          endif;
          if (EMPRESA_MASTER != $_SESSION['userlogin']['user_empresa']):
            $lv = 3;
            if (!APP_USERS || empty($userlogin) || $user_level >= $lv):
              ?>
              <div class="menu_section">
                <h3>Acesso Administrativo</h3>
                <ul class="nav side-menu">
                  <li class="<?php if (in_array('users', $linkto)) echo 'active'; ?>"><a><i class="fa fa-user"></i> Usuários <span class="label label-danger">ADM</span> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php if (in_array('users', $linkto)) echo 'display: block'; ?>">
                      <li class="<?php if (in_array('create', $linkto) && in_array('users', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=users/create">Cadastrar</a></li>
                      <li class="<?php if (in_array('index', $linkto) && in_array('users', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=users/index">Listar</a></li>
                    </ul>
                  </li>
                  <li class="<?php if (in_array('historico', $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=historico/index"><i class="fa fa-th-list"></i> Histórico <span class="label label-danger">ADM</span></a></li>
                </ul>
              </div>
            <?php endif; ?>
            <div class="menu_section">
              <h3>Recursos</h3>
              <ul class="nav side-menu">
                <?php
        // Vetor do menu de recursos com sub-menus nivel 1
                include('inc/menu-categorizado.inc.php');
                foreach ($CategMenu as $Chaves):
                  ?>
                  <li class="<?php if (in_array($Chaves['pasta'], $linkto)) echo 'active'; ?>"><a><i class="<?= $Chaves['icone']; ?>"></i> <?= $Chaves['titulo']; ?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php if (in_array($Chaves['pasta'], $linkto)) echo 'display: block'; ?>">
                      <?php foreach ($Chaves['url'] as $urls): ?>
                        <li class="<?php if (in_array($Chaves['pasta'], $linkto) && in_array($urls['pagina'], $linkto)) echo 'current-page'; ?>"><a href="painel.php?exe=<?= $Chaves['pasta'] . '/' . $urls['pagina'] ?>"><?= $urls['titulo']; ?></a></li>
                      <?php endforeach; ?>
                    </ul>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>
        </div>
        <!-- /sidebar menu -->
      </div>
    </div>
  </aside>
<!-- Main Sidebar end-->