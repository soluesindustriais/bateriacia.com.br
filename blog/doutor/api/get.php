<?php
header('Content-Type: application/json');
define('IS_API', true);

// Inclua o arquivo de configuração
$configPath = '../_app/Config.inc.php';


if (file_exists($configPath)) {
    require_once $configPath;
} else {
    http_response_code(500); // Erro interno do servidor
    echo json_encode(['error' => 'Arquivo de configuração não encontrado!']);
    exit;
}

// Verifica o método de requisição
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    http_response_code(405); // Método não permitido
    echo json_encode(['error' => 'Método não permitido. Use GET.']);
    exit;
}

// Obtém os parâmetros da URL ou do formulário
$blogId = $_GET['blog_id'] ?? $_POST['blog_id'] ?? null;
$catParent = $_GET['cat_parent'] ?? $_POST['cat_parent'] ?? null;
$limit = $_GET['limit'] ?? $_POST['limit'] ?? 10; // Limite padrão

// Valida o limite
$validLimits = [5, 25, 50, 100];
if (!in_array((int)$limit, $validLimits)) {
    $limit = 10; // Define um padrão se o limite não for válido
}

// Cria a instância da classe Read
$read = new Read();

try {
    if ($blogId) {
        // Retorna um post específico
        $read->ExeRead('dr_blog', 'WHERE blog_id = :id', "id={$blogId}");
        $result = $read->getResult();
        
        if ($result) {
            http_response_code(200); // OK
            echo json_encode($result);
        } else {
            http_response_code(404); // Não encontrado
            echo json_encode(['error' => 'Postagem não encontrada.']);
        }
    } elseif ($catParent) {
        // Retorna todos os posts de uma categoria com limite
        $read->ExeRead('dr_blog', 'WHERE cat_parent = :cat ORDER BY blog_date DESC LIMIT :limit', "cat={$catParent}&limit={$limit}");
        $result = $read->getResult();
        
        if ($result) {
            http_response_code(200); // OK
            echo json_encode($result);
        } else {
            http_response_code(404); // Não encontrado
            echo json_encode(['error' => 'Nenhuma postagem encontrada para esta categoria.']);
        }
    } else {
        http_response_code(400); // Requisição inválida
        echo json_encode(['error' => 'Parâmetros inválidos. Informe blog_id ou cat_parent.']);
    }
} catch (Exception $e) {
    http_response_code(500); // Erro interno do servidor
    echo json_encode(['error' => 'Erro ao processar a requisição: ' . $e->getMessage()]);
}
