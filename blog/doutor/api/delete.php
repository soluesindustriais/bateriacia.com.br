<?php
header('Content-Type: application/json');
define('IS_API', true);

// Inclua o arquivo de configuração
$currentDirectory = dirname(__DIR__);
$configPath = '../_app/Config.inc.php';


if (file_exists($configPath)) {
    require_once $configPath;
} else {
    http_response_code(500); // Erro interno do servidor
    echo json_encode(['error' => 'Arquivo de configuração não encontrado!']);
    exit;
}

// Verifica o método de requisição
$method = $_SERVER['REQUEST_METHOD'];

if ($method === 'DELETE') {
    // Obtenha os parâmetros da query string
    $postId = $_GET['blog_id'] ?? null;  // Agora usamos $_GET para capturar o ID
    $token = $_GET['token'] ?? null;  // Captura o token via $_GET

    // Verifica se o blog_id e o token foram passados
    if (is_null($postId) || !is_numeric($postId)) {
        http_response_code(400); // Requisição inválida
        echo json_encode(['error' => 'ID da postagem (blog_id) inválido ou não informado.']);
        exit;
    }

    if ($token !== TOKEN_API) {
        http_response_code(401); // Não autorizado
        echo json_encode(['error' => 'Acesso negado. Token inválido.']);
        exit;
    }

    // Tenta deletar a postagem
    try {
        $blog = new Blog();
        $blog->ExeDelete($postId);

        if ($blog->getResult()) {
            http_response_code(200); // Sucesso
            echo json_encode(['success' => 'Postagem deletada com sucesso!']);
        } else {
            http_response_code(500); // Erro interno do servidor
            echo json_encode(['error' => $blog->getError()]);
        }
    } catch (Exception $e) {
        http_response_code(500); // Erro interno do servidor
        echo json_encode(['error' => 'Erro ao deletar postagem: ' . $e->getMessage()]);
    }

} else {
    // Se o método não for DELETE
    http_response_code(405); // Método não permitido
    echo json_encode(['error' => 'Método não permitido. Use DELETE.']);
}
