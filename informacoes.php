<?$h1 = 'Informações'; $title = 'Informações - Bateria Cia'; $desc = 'Bateria Cia - Saiba tudo sobre Baterias. Faça cotações com diversas empresas de Baterias gratuitamente'; include('inc/head.php'); include('inc/fancy.php');?>
</head>

<body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminho?><h1><?=$h1?></h1>
                    <article class="full">
                        <ul class="thumbnails-main">
                            <li><a href="<?=$url?>bateria-60-amperes" title="Bateria 60 amperes"><img
                                        src="imagens/mpi/thumbs/Bateria-60-amperes-01.jpg" alt="Bateria 60 amperes"
                                        title="Bateria 60 amperes" /></a>
                                <h2><a href="<?=$url?>bateria-60-amperes" title="Bateria 60 amperes">Bateria 60
                                        amperes</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-automotiva" title="Bateria automotiva"><img
                                        src="imagens/mpi/thumbs/Bateria-automotiva-01.jpg" alt="Bateria automotiva"
                                        title="Bateria automotiva" /></a>
                                <h2><a href="<?=$url?>bateria-automotiva" title="Bateria automotiva">Bateria
                                        automotiva</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-para-caminhao" title="Bateria para caminhão"><img
                                        src="imagens/mpi/thumbs/Bateria-para-caminhao-01.jpg"
                                        alt="Bateria para caminhão" title="Bateria para caminhão" /></a>
                                <h2><a href="<?=$url?>bateria-para-caminhao" title="Bateria para caminhão">Bateria para
                                        caminhão</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-de-carro-preco" title="Bateria de carro preço"><img
                                        src="imagens/mpi/thumbs/Bateria-de-carro-preco-01.jpg"
                                        alt="Bateria de carro preço" title="Bateria de carro preço" /></a>
                                <h2><a href="<?=$url?>bateria-de-carro-preco" title="Bateria de carro preço">Bateria de
                                        carro preço</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-de-trator" title="Bateria de trator"><img
                                        src="imagens/mpi/thumbs/Bateria-de-trator-01.jpg" alt="Bateria de trator"
                                        title="Bateria de trator" /></a>
                                <h2><a href="<?=$url?>bateria-de-trator" title="Bateria de trator">Bateria de trator</a>
                                </h2>
                            </li>
                            <li><a href="<?=$url?>bateria-estacionaria" title="Bateria estacionaria"><img
                                        src="imagens/mpi/thumbs/Bateria-estacionaria-01.jpg" alt="Bateria estacionaria"
                                        title="Bateria estacionaria" /></a>
                                <h2><a href="<?=$url?>bateria-estacionaria" title="Bateria estacionaria">Bateria
                                        estacionaria</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-estacionaria-a-venda"
                                    title="Bateria estacionária a venda"><img
                                        src="imagens/mpi/thumbs/Bateria-estacionaria-a-venda-01.jpg"
                                        alt="Bateria estacionária a venda" title="Bateria estacionária a venda" /></a>
                                <h2><a href="<?=$url?>bateria-estacionaria-a-venda"
                                        title="Bateria estacionária a venda">Bateria estacionária a venda</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-estacionaria-comprar"
                                    title="Bateria estacionária comprar"><img
                                        src="imagens/mpi/thumbs/Bateria-estacionaria-comprar-01.jpg"
                                        alt="Bateria estacionária comprar" title="Bateria estacionária comprar" /></a>
                                <h2><a href="<?=$url?>bateria-estacionaria-comprar"
                                        title="Bateria estacionária comprar">Bateria estacionária comprar</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-estacionaria-industrial"
                                    title="Bateria estacionaria industrial"><img
                                        src="imagens/mpi/thumbs/Bateria-estacionaria-industrial-01.jpg"
                                        alt="Bateria estacionaria industrial"
                                        title="Bateria estacionaria industrial" /></a>
                                <h2><a href="<?=$url?>bateria-estacionaria-industrial"
                                        title="Bateria estacionaria industrial">Bateria estacionaria industrial</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-estacionaria-onde-comprar"
                                    title="Bateria estacionária onde comprar"><img
                                        src="imagens/mpi/thumbs/Bateria-estacionaria-onde-comprar-01.jpg"
                                        alt="Bateria estacionária onde comprar"
                                        title="Bateria estacionária onde comprar" /></a>
                                <h2><a href="<?=$url?>bateria-estacionaria-onde-comprar"
                                        title="Bateria estacionária onde comprar">Bateria estacionária onde comprar</a>
                                </h2>
                            </li>
                            <li><a href="<?=$url?>bateria-estacionaria-preco" title="Bateria estacionaria preço"><img
                                        src="imagens/mpi/thumbs/Bateria-estacionaria-preco-01.jpg"
                                        alt="Bateria estacionaria preço" title="Bateria estacionaria preço" /></a>
                                <h2><a href="<?=$url?>bateria-estacionaria-preco"
                                        title="Bateria estacionaria preço">Bateria estacionaria preço</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-para-empilhadeira" title="Bateria para empilhadeira"><img
                                        src="imagens/mpi/thumbs/Bateria-para-empilhadeira-01.jpg"
                                        alt="Bateria para empilhadeira" title="Bateria para empilhadeira" /></a>
                                <h2><a href="<?=$url?>bateria-para-empilhadeira"
                                        title="Bateria para empilhadeira">Bateria para empilhadeira</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-para-empilhadeira-eletrica"
                                    title="Bateria para empilhadeira elétrica"><img
                                        src="imagens/mpi/thumbs/Bateria-para-empilhadeira-eletrica-01.jpg"
                                        alt="Bateria para empilhadeira elétrica"
                                        title="Bateria para empilhadeira elétrica" /></a>
                                <h2><a href="<?=$url?>bateria-para-empilhadeira-eletrica"
                                        title="Bateria para empilhadeira elétrica">Bateria para empilhadeira
                                        elétrica</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-para-empilhadeira-tracionaria"
                                    title="Bateria para empilhadeira tracionaria"><img
                                        src="imagens/mpi/thumbs/Bateria-para-empilhadeira-tracionaria-01.jpg"
                                        alt="Bateria para empilhadeira tracionaria"
                                        title="Bateria para empilhadeira tracionaria" /></a>
                                <h2><a href="<?=$url?>bateria-para-empilhadeira-tracionaria"
                                        title="Bateria para empilhadeira tracionaria">Bateria para empilhadeira
                                        tracionaria</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-tracionaria" title="Bateria tracionaria"><img
                                        src="imagens/mpi/thumbs/Bateria-tracionaria-01.jpg" alt="Bateria tracionaria"
                                        title="Bateria tracionaria" /></a>
                                <h2><a href="<?=$url?>bateria-tracionaria" title="Bateria tracionaria">Bateria
                                        tracionaria</a></h2>
                            </li>
                            <li><a href="<?=$url?>bateria-tracionaria-para-empilhadeira"
                                    title="Bateria tracionaria para empilhadeira"><img
                                        src="imagens/mpi/thumbs/Bateria-tracionaria-para-empilhadeira-01.jpg"
                                        alt="Bateria tracionaria para empilhadeira"
                                        title="Bateria tracionaria para empilhadeira" /></a>
                                <h2><a href="<?=$url?>bateria-tracionaria-para-empilhadeira"
                                        title="Bateria tracionaria para empilhadeira">Bateria tracionaria para
                                        empilhadeira</a></h2>
                            </li>
                            <li><a href="<?=$url?>baterias-automotivas-preco" title="Baterias automotivas preço"><img
                                        src="imagens/mpi/thumbs/Baterias-automotivas-preco-01.jpg"
                                        alt="Baterias automotivas preço" title="Baterias automotivas preço" /></a>
                                <h2><a href="<?=$url?>baterias-automotivas-preco"
                                        title="Baterias automotivas preço">Baterias automotivas preço</a></h2>
                            </li>
                            <li><a href="<?=$url?>baterias-de-automoveis-precos"
                                    title="Baterias de automóveis preços"><img
                                        src="imagens/mpi/thumbs/Baterias-de-automoveis-precos-01.jpg"
                                        alt="Baterias de automóveis preços" title="Baterias de automóveis preços" /></a>
                                <h2><a href="<?=$url?>baterias-de-automoveis-precos"
                                        title="Baterias de automóveis preços">Baterias de automóveis preços</a></h2>
                            </li>
                            <li><a href="<?=$url?>baterias-industriais" title="Baterias industriais"><img
                                        src="imagens/mpi/thumbs/Baterias-industriais-01.jpg" alt="Baterias industriais"
                                        title="Baterias industriais" /></a>
                                <h2><a href="<?=$url?>baterias-industriais" title="Baterias industriais">Baterias
                                        industriais</a></h2>
                            </li>
                            <li><a href="<?=$url?>baterias-de-caminhao" title="Baterias de caminhão"><img
                                        src="imagens/mpi/thumbs/Baterias-de-caminhao-01.jpg" alt="Baterias de caminhão"
                                        title="Baterias de caminhão" /></a>
                                <h2><a href="<?=$url?>baterias-de-caminhao" title="Baterias de caminhão">Baterias de
                                        caminhão</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-automotiva"
                                    title="Carregador de bateria automotiva"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-automotiva-01.jpg"
                                        alt="Carregador de bateria automotiva"
                                        title="Carregador de bateria automotiva" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-automotiva"
                                        title="Carregador de bateria automotiva">Carregador de bateria automotiva</a>
                                </h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-com-auxiliar-de-partida"
                                    title="Carregador de bateria com auxiliar de partida"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-com-auxiliar-de-partida-01.jpg"
                                        alt="Carregador de bateria com auxiliar de partida"
                                        title="Carregador de bateria com auxiliar de partida" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-com-auxiliar-de-partida"
                                        title="Carregador de bateria com auxiliar de partida">Carregador de bateria com
                                        auxiliar de partida</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-de-caminhao"
                                    title="Carregador de bateria de caminhão"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-de-caminhao-01.jpg"
                                        alt="Carregador de bateria de caminhão"
                                        title="Carregador de bateria de caminhão" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-de-caminhao"
                                        title="Carregador de bateria de caminhão">Carregador de bateria de caminhão</a>
                                </h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-de-carro"
                                    title="Carregador de bateria de carro"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-de-carro-01.jpg"
                                        alt="Carregador de bateria de carro"
                                        title="Carregador de bateria de carro" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-de-carro"
                                        title="Carregador de bateria de carro">Carregador de bateria de carro</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-de-carro-e-moto"
                                    title="Carregador de bateria de carro e moto"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-de-carro-e-moto-01.jpg"
                                        alt="Carregador de bateria de carro e moto"
                                        title="Carregador de bateria de carro e moto" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-de-carro-e-moto"
                                        title="Carregador de bateria de carro e moto">Carregador de bateria de carro e
                                        moto</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-estacionaria"
                                    title="Carregador de bateria estacionaria"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-estacionaria-01.jpg"
                                        alt="Carregador de bateria estacionaria"
                                        title="Carregador de bateria estacionaria" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-estacionaria"
                                        title="Carregador de bateria estacionaria">Carregador de bateria
                                        estacionaria</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-preco" title="Carregador de bateria preço"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-preco-01.jpg"
                                        alt="Carregador de bateria preço" title="Carregador de bateria preço" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-preco"
                                        title="Carregador de bateria preço">Carregador de bateria preço</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-de-bateria-veicular"
                                    title="Carregador de bateria veicular"><img
                                        src="imagens/mpi/thumbs/Carregador-de-bateria-veicular-01.jpg"
                                        alt="Carregador de bateria veicular"
                                        title="Carregador de bateria veicular" /></a>
                                <h2><a href="<?=$url?>carregador-de-bateria-veicular"
                                        title="Carregador de bateria veicular">Carregador de bateria veicular</a></h2>
                            </li>
                            <li><a href="<?=$url?>carregador-dessulfatador" title="Carregador dessulfatador"><img
                                        src="imagens/mpi/thumbs/Carregador-dessulfatador-01.jpg"
                                        alt="Carregador dessulfatador" title="Carregador dessulfatador" /></a>
                                <h2><a href="<?=$url?>carregador-dessulfatador"
                                        title="Carregador dessulfatador">Carregador dessulfatador</a></h2>
                            </li>
                            <li><a href="<?=$url?>comprar-bateria-automotiva" title="Comprar bateria automotiva"><img
                                        src="imagens/mpi/thumbs/Comprar-bateria-automotiva-01.jpg"
                                        alt="Comprar bateria automotiva" title="Comprar bateria automotiva" /></a>
                                <h2><a href="<?=$url?>comprar-bateria-automotiva"
                                        title="Comprar bateria automotiva">Comprar bateria automotiva</a></h2>
                            </li>
                            <li><a href="<?=$url?>comprar-bateria-de-carro" title="Comprar bateria de carro"><img
                                        src="imagens/mpi/thumbs/Comprar-bateria-de-carro-01.jpg"
                                        alt="Comprar bateria de carro" title="Comprar bateria de carro" /></a>
                                <h2><a href="<?=$url?>comprar-bateria-de-carro" title="Comprar bateria de carro">Comprar
                                        bateria de carro</a></h2>
                            </li>
                            <li><a href="<?=$url?>descarregador-de-baterias" title="Descarregador de baterias"><img
                                        src="imagens/mpi/thumbs/Descarregador-de-baterias-01.jpg"
                                        alt="Descarregador de baterias" title="Descarregador de baterias" /></a>
                                <h2><a href="<?=$url?>descarregador-de-baterias"
                                        title="Descarregador de baterias">Descarregador de baterias</a></h2>
                            </li>
                            <li><a href="<?=$url?>dessulfatador-de-bateria" title="Dessulfatador de bateria"><img
                                        src="imagens/mpi/thumbs/Dessulfatador-de-bateria-01.jpg"
                                        alt="Dessulfatador de bateria" title="Dessulfatador de bateria" /></a>
                                <h2><a href="<?=$url?>dessulfatador-de-bateria"
                                        title="Dessulfatador de bateria">Dessulfatador de bateria</a></h2>
                            </li>
                            <li><a href="<?=$url?>dessulfatador-eletronico" title="Dessulfatador eletrônico"><img
                                        src="imagens/mpi/thumbs/Dessulfatador-eletronico-01.jpg"
                                        alt="Dessulfatador eletrônico" title="Dessulfatador eletrônico" /></a>
                                <h2><a href="<?=$url?>dessulfatador-eletronico"
                                        title="Dessulfatador eletrônico">Dessulfatador eletrônico</a></h2>
                            </li>
                            <li><a href="<?=$url?>fabrica-de-baterias-automotivas"
                                    title="Fábrica de baterias automotivas"><img
                                        src="imagens/mpi/thumbs/Fabrica-de-baterias-automotivas-01.jpg"
                                        alt="Fábrica de baterias automotivas"
                                        title="Fábrica de baterias automotivas" /></a>
                                <h2><a href="<?=$url?>fabrica-de-baterias-automotivas"
                                        title="Fábrica de baterias automotivas">Fábrica de baterias automotivas</a></h2>
                            </li>
                            <li><a href="<?=$url?>fabricante-de-baterias-para-caminhao"
                                    title="Fabricante de baterias para caminhão"><img
                                        src="imagens/mpi/thumbs/Fabricante-de-baterias-para-caminhao-01.jpg"
                                        alt="Fabricante de baterias para caminhão"
                                        title="Fabricante de baterias para caminhão" /></a>
                                <h2><a href="<?=$url?>fabricante-de-baterias-para-caminhao"
                                        title="Fabricante de baterias para caminhão">Fabricante de baterias para
                                        caminhão</a></h2>
                            </li>
                            <li><a href="<?=$url?>fornecedor-de-bateria-estacionaria"
                                    title="Fornecedor de bateria estacionaria"><img
                                        src="imagens/mpi/thumbs/Fornecedor-de-bateria-estacionaria-01.jpg"
                                        alt="Fornecedor de bateria estacionaria"
                                        title="Fornecedor de bateria estacionaria" /></a>
                                <h2><a href="<?=$url?>fornecedor-de-bateria-estacionaria"
                                        title="Fornecedor de bateria estacionaria">Fornecedor de bateria
                                        estacionaria</a></h2>
                            </li>
                            <li><a href="<?=$url?>fornecedor-de-baterias-automotivas"
                                    title="Fornecedor de baterias automotivas"><img
                                        src="imagens/mpi/thumbs/Fornecedor-de-baterias-automotivas-01.jpg"
                                        alt="Fornecedor de baterias automotivas"
                                        title="Fornecedor de baterias automotivas" /></a>
                                <h2><a href="<?=$url?>fornecedor-de-baterias-automotivas"
                                        title="Fornecedor de baterias automotivas">Fornecedor de baterias
                                        automotivas</a></h2>
                            </li>
                            <li><a href="<?=$url?>fornecedor-de-baterias-para-caminhao"
                                    title="Fornecedor de baterias para caminhão"><img
                                        src="imagens/mpi/thumbs/Fornecedor-de-baterias-para-caminhao-01.jpg"
                                        alt="Fornecedor de baterias para caminhão"
                                        title="Fornecedor de baterias para caminhão" /></a>
                                <h2><a href="<?=$url?>fornecedor-de-baterias-para-caminhao"
                                        title="Fornecedor de baterias para caminhão">Fornecedor de baterias para
                                        caminhão</a></h2>
                            </li>
                            <li><a href="<?=$url?>loja-de-baterias" title="Loja de baterias"><img
                                        src="imagens/mpi/thumbs/Loja-de-baterias-01.jpg" alt="Loja de baterias"
                                        title="Loja de baterias" /></a>
                                <h2><a href="<?=$url?>loja-de-baterias" title="Loja de baterias">Loja de baterias</a>
                                </h2>
                            </li>
                            <li><a href="<?=$url?>loja-de-baterias-automotivas"
                                    title="Loja de baterias automotivas"><img
                                        src="imagens/mpi/thumbs/Loja-de-baterias-automotivas-01.jpg"
                                        alt="Loja de baterias automotivas" title="Loja de baterias automotivas" /></a>
                                <h2><a href="<?=$url?>loja-de-baterias-automotivas"
                                        title="Loja de baterias automotivas">Loja de baterias automotivas</a></h2>
                            </li>
                            <li><a href="<?=$url?>loja-de-baterias-de-caminhao"
                                    title="Loja de baterias de caminhão"><img
                                        src="imagens/mpi/thumbs/Loja-de-baterias-de-caminhao-01.jpg"
                                        alt="Loja de baterias de caminhão" title="Loja de baterias de caminhão" /></a>
                                <h2><a href="<?=$url?>loja-de-baterias-de-caminhao"
                                        title="Loja de baterias de caminhão">Loja de baterias de caminhão</a></h2>
                            </li>
                            <li><a href="<?=$url?>onde-comprar-bateria-de-carro"
                                    title="Onde comprar bateria de carro"><img
                                        src="imagens/mpi/thumbs/Onde-comprar-bateria-de-carro-01.jpg"
                                        alt="Onde comprar bateria de carro" title="Onde comprar bateria de carro" /></a>
                                <h2><a href="<?=$url?>onde-comprar-bateria-de-carro"
                                        title="Onde comprar bateria de carro">Onde comprar bateria de carro</a></h2>
                            </li>
                            <li><a href="<?=$url?>preco-carregador-de-bateria-de-carro"
                                    title="Preço carregador de bateria de carro"><img
                                        src="imagens/mpi/thumbs/Preco-carregador-de-bateria-de-carro-01.jpg"
                                        alt="Preço carregador de bateria de carro"
                                        title="Preço carregador de bateria de carro" /></a>
                                <h2><a href="<?=$url?>preco-carregador-de-bateria-de-carro"
                                        title="Preço carregador de bateria de carro">Preço carregador de bateria de
                                        carro</a></h2>
                            </li>
                            <li><a href="<?=$url?>preco-de-bateria-60-amperes" title="Preço de bateria 60 amperes"><img
                                        src="imagens/mpi/thumbs/Preco-de-bateria-60-amperes-01.jpg"
                                        alt="Preço de bateria 60 amperes" title="Preço de bateria 60 amperes" /></a>
                                <h2><a href="<?=$url?>preco-de-bateria-60-amperes"
                                        title="Preço de bateria 60 amperes">Preço de bateria 60 amperes</a></h2>
                            </li>
                            <li><a href="<?=$url?>preco-de-bateria-automotiva" title="Preço de bateria automotiva"><img
                                        src="imagens/mpi/thumbs/Preco-de-bateria-automotiva-01.jpg"
                                        alt="Preço de bateria automotiva" title="Preço de bateria automotiva" /></a>
                                <h2><a href="<?=$url?>preco-de-bateria-automotiva"
                                        title="Preço de bateria automotiva">Preço de bateria automotiva</a></h2>
                            </li>
                            <li><a href="<?=$url?>preco-de-bateria-para-automovel"
                                    title="Preço de bateria para automóvel"><img
                                        src="imagens/mpi/thumbs/Preco-de-bateria-para-automovel-01.jpg"
                                        alt="Preço de bateria para automóvel"
                                        title="Preço de bateria para automóvel" /></a>
                                <h2><a href="<?=$url?>preco-de-bateria-para-automovel"
                                        title="Preço de bateria para automóvel">Preço de bateria para automóvel</a></h2>
                            </li>
                            <li><a href="<?=$url?>venda-de-bateria-de-carro" title="Venda de bateria de carro"><img
                                        src="imagens/mpi/thumbs/Venda-de-bateria-de-carro-01.jpg"
                                        alt="Venda de bateria de carro" title="Venda de bateria de carro" /></a>
                                <h2><a href="<?=$url?>venda-de-bateria-de-carro" title="Venda de bateria de carro">Venda
                                        de bateria de carro</a></h2>
                            </li>
                            <li><a href="<?=$url?>venda-de-bateria-para-empilhadeira"
                                    title="Venda de bateria para empilhadeira"><img
                                        src="imagens/mpi/thumbs/Venda-de-bateria-para-empilhadeira-01.jpg"
                                        alt="Venda de bateria para empilhadeira"
                                        title="Venda de bateria para empilhadeira" /></a>
                                <h2><a href="<?=$url?>venda-de-bateria-para-empilhadeira"
                                        title="Venda de bateria para empilhadeira">Venda de bateria para
                                        empilhadeira</a></h2>
                            </li>
                            <li><a href="<?=$url?>venda-de-bateria-tracionaria"
                                    title="Venda de bateria tracionaria"><img
                                        src="imagens/mpi/thumbs/Venda-de-bateria-tracionaria-01.jpg"
                                        alt="Venda de bateria tracionaria" title="Venda de bateria tracionaria" /></a>
                                <h2><a href="<?=$url?>venda-de-bateria-tracionaria"
                                        title="Venda de bateria tracionaria">Venda de bateria tracionaria</a></h2>
                            </li>
                        </ul>
                    </article>
                </section>
            </div>
        </main>
    </div>
    <? include('inc/footer.php');?>
</body>

</html>